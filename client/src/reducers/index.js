import {combineReducers} from 'redux';

import authReducer from './authReducer';
import errorReducrer from './errorReducer'
import profileReducer from './profileReducer';

const rootReducer = combineReducers({
    auth: authReducer,
    errors: errorReducrer,
    profile: profileReducer
});

export default rootReducer;