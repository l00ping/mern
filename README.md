This is my first project with React and node.js, I hope you will like it!

http://pierrelouisrebours.com

/!\ IN PROGRESS /!\

- Build a full stack social network app with React, Redux, Node, Express & MongoDB
- Create an extensive backend API with Express
- Use Stateless JWT authentication practices
- Integrate React with an Express backend in an elegant way
- Use Redux for state management